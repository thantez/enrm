# enrm

a tiny shell script to remove "-en" in .srt files

# Installation
after cloning this repo,
run `source ./install.sh`

# Usage
when you see a subtitle with "-en" in its name, just run
`sudo enrm `
